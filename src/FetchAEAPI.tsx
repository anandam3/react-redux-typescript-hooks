import React from "react";
import { useSelector } from "react-redux";
import { InitialState } from "./store/root-reducer";

import { Layout, Table } from "antd";

const { Content } = Layout;

interface StateProps {
  API: any;
}

const FetchAEAPI: React.FC = () => {
  // Redux state
  const { API } = useSelector<InitialState, StateProps>(
    (state: InitialState) => {
      return {
        API: state.API
      };
    }
  );

  // Define columns
  const columns = [
    {
      title: "Time",
      dataIndex: "time",
      key: "time"
    },
    {
      title: "Count",
      dataIndex: "count",
      key: "count"
    }
  ];

  return (
    <div>
      <Content style={{ padding: "0 50px" }}>
        <div
          className="site-layout-content"
          style={{ background: "#fff", padding: 24, minHeight: 600 }}
        >
          <Table
            dataSource={
              API &&
              API.map((el: any, key: number) => Object.assign({ ...el, key }))
            }
            columns={columns}
          />
        </div>
      </Content>
    </div>
  );
};

export default FetchAEAPI;
