import React from "react";
import { Chart, Geom, Axis, Tooltip, Coord } from "bizcharts";
import { useSelector } from "react-redux";
import { InitialState } from "./store/root-reducer";

interface StateProps {
  API: any;
}

const BarChart: React.FC = () => {
  // Redux state
  const { API } = useSelector<InitialState, StateProps>(
    (state: InitialState) => {
      return {
        API: state.API
      };
    }
  );

  const cols = {
    count: { min: 0 },
    time: { range: [0, 0.95] }
  };

  return (
    <div>
      <Chart height={600} data={API} scale={cols} forceFit>
        <Coord transpose />
        <Axis name="time" />
        <Axis name="count" visible={true} />
        <Tooltip />
        <Geom
          type="interval"
          position="time*count"
          color={["count", "darkblue"]}
        ></Geom>
      </Chart>
    </div>
  );
};

export default BarChart;
