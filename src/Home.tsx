import React from "react";
import { Route, Link, BrowserRouter } from "react-router-dom";
import ChartAEAPI from "./ChartAEAPI";
import FetchAEAPI from "./FetchAEAPI";

const Home: React.FC = () => {
  return (
    <BrowserRouter>
      <div>
        <Route path="/ae" component={FetchAEAPI} />
        <div>
          <Link to="/ae/">Adverse Event: Time vs Count </Link>
        </div>
        <Route path="/bar" component={ChartAEAPI} />
        <div>
          <Link to="/bar/">Bar Chart </Link>
        </div>
      </div>
    </BrowserRouter>
  );
};

export default Home;
