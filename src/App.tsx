import React from "react";
import "./App.css";
import { store } from "./store";
import { Provider } from "react-redux";
import { Layout } from "antd";
import ReduxHooksComponent from "./ReduxHooksComponent";

const App: React.FC = () => {
  return (
    <Provider store={store}>
      <div className="App">
        <Layout>
          <div className="site-card-wrapper">
            <ReduxHooksComponent />
          </div>
        </Layout>
      </div>
    </Provider>
  );
};

export default App;
