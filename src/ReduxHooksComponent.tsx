import React, { useEffect } from "react";
import { Card, Button, Row, Col, Divider } from "antd";
import FetchAEAPI from "./FetchAEAPI";
import LineChart from "./LineChart";
import BarChart from "./BarChart";
import axios from "axios";
import { useDispatch } from "react-redux";
import { RootDispatcher } from "./store/root-redux";

interface Props {}

interface StateProps {
  API: any;
}

const ReduxHooksComponent: React.FC<Props> = () => {
  const dispatch = useDispatch();
  const rootDispatcher = new RootDispatcher(dispatch);

  // To fetch API
  useEffect(() => {
    fetchData();
  });

  const fetchData = async () => {
    await axios
      .get(
        "https://api.fda.gov/drug/event.json?search=receivedate:[20040101+TO+20040201]&count=receivedate"
      )
      .then(response => {
        rootDispatcher.callAPI(response.data.results);
      });
  };

  return (
    <Row gutter={[16, 24]}>
      <Col span={24}>
        <Button type="primary" size="large" onClick={() => fetchData()}>
          Call API
        </Button>
        <Button size="large" onClick={rootDispatcher.deleteAPI}>
          Delete API
        </Button>
        <Divider orientation="left"></Divider>
      </Col>
      <Col span={24}>
        <Card title="Adverse Event: Time vs Count" bordered={true}>
          <FetchAEAPI />
        </Card>
      </Col>
      <Row>
        <Col span={10}>
          <Card title="Bar Chart" bordered={true}>
            <BarChart />
          </Card>
        </Col>
        <Col span={14}>
          <Card title="Line Chart" bordered={true}>
            <LineChart />
          </Card>
        </Col>
      </Row>
    </Row>
  );
};

export default ReduxHooksComponent;
