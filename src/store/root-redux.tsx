import { Dispatch } from "redux";
import { DispatchAction, ActionType } from "./root-reducer";

export class RootDispatcher {
  private readonly dispatch: Dispatch<DispatchAction>;

  constructor(dispatch: Dispatch<DispatchAction>) {
    this.dispatch = dispatch;
  }

  callAPI = (API: any) =>
    this.dispatch({ type: ActionType.CallAPI, payload: { API } });

  deleteAPI = () => this.dispatch({ type: ActionType.DeleteAPI, payload: {} });
}
