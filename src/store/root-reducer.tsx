import { Action, Reducer } from "redux";

export enum ActionType {
  CallAPI = "CALL_API",
  DeleteAPI = "DELETE_API"
}

export interface InitialState {
  API: any;
}

export const initialState: InitialState = {
  API: null
};

export interface DispatchAction extends Action<ActionType> {
  payload: Partial<InitialState>;
}

export const rootReducer: Reducer<InitialState, DispatchAction> = (
  state = initialState,
  action
) => {
  console.log(state);
  if (action.type === ActionType.CallAPI) {
    return { ...state, API: action.payload.API };
  } else if (action.type === ActionType.DeleteAPI) {
    return { ...state, API: "" };
  } else return state;
};
