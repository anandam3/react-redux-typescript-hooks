import React, { useState, useEffect } from "react";
import { Chart } from "@antv/g2";
import axios from "axios";

const ChartAEAPI: React.FC = () => {
  const [data, setData] = useState([]);

  useEffect(() => {
    const fetchData = async () => {
      const result = await axios(
        "https://api.fda.gov/drug/event.json?search=receivedate:[20040101+TO+20040204]&count=receivedate"
      );
      setData(result.data.results);
    };

    fetchData();
  }, []);

  const chart = new Chart({
    container: "root",
    width: 2000,
    height: 300
  });
  chart.data(data);
  chart.scale("count", { nice: true });
  chart.coordinate().transpose();
  chart.tooltip({
    showMarkers: false
  });
  chart.interaction("active-region");
  chart.interval().position("time*count");
  chart.render();
  return <div></div>;
};

export default ChartAEAPI;
