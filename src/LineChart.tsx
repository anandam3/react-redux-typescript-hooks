import React from "react";
import { Chart, Axis, Tooltip, Geom, Legend } from "bizcharts";
import { useSelector } from "react-redux";
import { InitialState } from "./store/root-reducer";

interface StateProps {
  API: any;
}

const cols = {
  count: { min: 0 },
  time: { range: [0, 0.95] }
};

const LineChart: React.FC = () => {
  // Redux state
  const { API } = useSelector<InitialState, StateProps>(
    (state: InitialState) => {
      return {
        API: state.API
      };
    }
  );

  return (
    <Chart width={1800} height={400} data={API} scale={cols} forceFit>
      <Axis name="time" />
      <Axis name="count" />
      <Legend />
      <Tooltip crosshairs={{ type: "y" }} />
      <Geom type="line" position="time*count" size={5} color="red" />
      <Geom
        type="point"
        position="time*count"
        size={4}
        shape={"circle"}
        style={{ stroke: "#fff", lineWidth: 1 }}
        color="type"
      />
    </Chart>
  );
};

export default LineChart;
